# Make Your Burger! (Vue 3)

Projeto para registro do aprendizado de Vue.js através do [Curso de Vue 3](https://www.youtube.com/watch?v=wsAQQioPIJs&list=PLnDvRpP8BnezDglaAvtWgQXzsOmXUuRHL) do professor Matheus Battisti - Hora de Codar, no Youtube.

Neste projeto vamos desenvolver um sistema para registro de pedidos de hamburger(CRUD) e manipulação de dados dentro da lógica de negócios.

Tenologias: HTML, CSS, Javascript ES6, Vue3, node, npm.
Módulos adicionais npm: Vue CLI, Vue Router, Vuex.